<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii;
use yii\helpers\ArrayHelper;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	
	public static function  tableName (){
		return 'user';
	}
	
	public function rules() //Validation
	{
		return
		
		[
			[['username','password','auth_key',], 'string', 'max' => 255],     //type of field
			[['username','password',], 'required'],								//required
			[['username',], 'unique'],										//username type
		];
	}
    /**
     * @inheritdoc
     */
    public static function findIdentity($id) //מחזירה את האובייקט עם כל הפרטים שלו
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException ('Not supported');
		
		return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    /*public function validatePassword($password)
    {
        return $this->password === $password;  //dtabase==login
    }*/
	
	//change the previous function - line 85
	public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);;
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	//hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord) //input value to auth_key
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	//create fullname pseuodo field
	public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
	//use yii\helpers\ArrayHelper;
	//A method to get an array of all users models/User
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'fullname');
		return $users;						
	}
}
