<?php

namespace app\models;

use yii\db\ActiveRecord;

class Student extends ActiveRecord //שם המחלקה חייב להיות זהה לשם הקובץ
{

	public static function tablename(){
		return 'student';
	}
	
	public static function getName($id){
		
		$student = self::findOne($id);
		
		isset ($student)?
		$return = $student->name:
		$return = "No Student found with id $id"; //false
		return $return;
	}
}
