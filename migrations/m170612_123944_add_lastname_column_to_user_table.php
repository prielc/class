<?php

use yii\db\Migration;

/**
 * Handles adding lastname to table `user`.
 */
class m170612_123944_add_lastname_column_to_user_table extends Migration
{
    public function up()
    {
		$this->addColumn('user','firstname','string');
		$this->addColumn('user','lastname','string');
    }

    public function down()
    {
        $this->dropColumn('user','firstname');
		$this->dropColumn('user','lastname');
    }
	
}
