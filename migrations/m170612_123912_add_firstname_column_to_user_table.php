<?php

use yii\db\Migration;

/**
 * Handles adding firstname to table `user`.
 */
class m170612_123912_add_firstname_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'firstname', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'firstname');
    }
}
