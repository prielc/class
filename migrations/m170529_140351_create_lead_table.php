<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lead`.
 */
class m170529_140351_create_lead_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lead', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'notes' => $this->text(),
            'status' => $this->integer(),
            'owner' => $this->integer(),
            'created_at' => $this->integer(),
            'update_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lead');
    }
}
